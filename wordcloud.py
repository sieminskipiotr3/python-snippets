from wordcloud import WordCloud
wordcloud = WordCloud().generate(' '.join(INSERT DATAFRAME COLUMN))
# plot the WordCloud image
plt.figure(figsize = (10, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.show()
