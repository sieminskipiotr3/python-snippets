# this is an example operation on test datasets showing the way algorithm works
# can be re-adjusted to any sort of matching you required and then filteres by scores according to your needs
# JaroWinkler measures the distance between strings; best used for similar words with typos match
# cosine similarity returns angle on three-dimensional space between strings, best used for similar phrases match
# ratcliff measures the difference between the tokens in the strings
# article with detailed explanations: https://itnext.io/string-similarity-the-basic-know-your-algorithms-guide-3de3d7346227

import pandas as pd

from similarity.jarowinkler import JaroWinkler
from similarity.cosine import Cosine
import textdistance


df1 = pd.DataFrame({
    "name": ["mahesh", "suresh"]
})

df2 = pd.DataFrame({
    "name": ["mahesh", "surendra", "shrivatsa", "suresh", "maheshwari"]
})

df = pd.MultiIndex.from_product(
    [df1["name"], df2["name"]], names=["col1", "col2"]
).to_frame(index=False)

jarowinkler = JaroWinkler()
df["jarowinkler_sim"] = [jarowinkler.similarity(i,j) for i,j in zip(df["col1"],df["col2"])]

df['ratcliff'] = [textdistance.ratcliff_obershelp(x,y) for x,y in zip(df['col1'], df['col2'])]

cosine = Cosine(2)
df["p0"] = df["col1"].apply(lambda s: cosine.get_profile(s)) 
df["p1"] = df["col2"].apply(lambda s: cosine.get_profile(s)) 
df["cosine_sim"] = [cosine.similarity_profiles(p0,p1) for p0,p1 in zip(df["p0"],df["p1"])]

df.drop(["p0", "p1"], axis=1)
